part of gmq;

AssetManager assetManager;
Random rand = Random();

class Game extends BaseGame {

  Renderer renderer;
  World world;

  @override
  create() {
    world = new Opening(this, assetManager.get("atlas"), "opening.tmx");
    renderer = new Renderer(world, gl, width, height);
    document.getElementById("loading").remove();
    fadeIn();
  }

  @override
  preload() {
    assetManager = this.assetManager;
    assetManager.load("atlas", loadAtlas("img/atlas.atlas", loadTexture(gl, "img/atlas.png", nearest)));
    assetManager.load("blackbox.tmx", loadTilemap("map/blackbox.tmx", atlas: assetManager.getLoading("atlas")));
    assetManager.load("opening.tmx", loadTilemap("map/opening.tmx", atlas: assetManager.getLoading("atlas")));
    assetManager.load("boost", loadSound(audio, "snd/boost.wav"));
    assetManager.load("move", loadSound(audio, "snd/move.wav"));
    assetManager.load("pickup", loadSound(audio, "snd/pickup.wav"));
    assetManager.load("factory", loadSound(audio, "snd/factory.wav"));
    assetManager.load("font", loadFont("img/m5x7.fnt", loadTexture(gl, "img/m5x7.png", nearest)));
    assetManager.load("doublebeep", loadSound(audio, "snd/doublebeep.wav"));
    assetManager.load("powerup", loadSound(audio, "snd/powerup.wav"));
  }

  @override
  render(num delta) {
    renderer.render(world, delta);
  }

  @override
  update(num delta) {
    world.update(delta);
  }

  @override
  config() {
    scaleMode = ScaleMode.resize;
  }

  @override
  resize(num width, num height) {
    super.resize(width, height);
    renderer.resize(width, height);
  }

  fadeIn() {
    new Tween()
      ..get = [() => renderer.fadeAmount]
      ..set = [(value) => renderer.fadeAmount = value]
      ..target = [0.0]
      ..duration = 0.5
      ..ease = Ease.quadIn
      ..start(tweenManager);
  }

  fadeOut() {
    new Tween()
      ..get = [() => renderer.fadeAmount]
      ..set = [(value) => renderer.fadeAmount = value]
      ..target = [1.0]
      ..duration = 0.5
      ..ease = Ease.quadOut
      ..start(tweenManager);
  }

}