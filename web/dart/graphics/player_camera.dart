part of gmq;

const double HORIZ_SCROLL_SPEED = 200;
const double VERT_SCROLL_SPEED = 500;

class PlayerCamera {

  Camera2D internal;

  num worldWidth, worldHeight;

  World world;

  Entity player;

  PlayerCamera(this.world, this.player, this.worldWidth, this.worldHeight) {
    internal = Camera2D.originCenter(worldWidth, worldHeight);
    internal.transform.translation = player.box.center;
  }

  update(double delta) {
    internal.transform.translation = player.box.center;
    if(internal.transform.x - worldWidth / 2 < 0) {
      internal.transform.x = worldWidth / 2;
    }
    if(internal.transform.x + worldWidth / 2 > world.map.width * TILE_SIZE) {
      internal.transform.x = world.map.width * TILE_SIZE - worldWidth / 2;
    }
    if(internal.transform.y - worldHeight / 2 < 0) {
      internal.transform.y = worldHeight / 2;
    }
    if(internal.transform.y + worldHeight / 2 > world.map.height * TILE_SIZE) {
      internal.transform.y = world.map.height * TILE_SIZE - worldHeight / 2;
    }
    internal.update();
  }

  Matrix4 get combined => internal.combined;

  Vector2 get playerScreenPos =>
      player.box.center - Vector2(internal.transform.x, internal.transform.y);
  double get playerScreenX => playerScreenPos.x;
  double get playerScreenY => playerScreenPos.y;

}