part of gmq;

const int TILE_SIZE = 8;
const int TILE_HEIGHT = 10;

class Renderer {
  World world;

  GLWrapper gl;

  num screenWidth;
  num screenHeight;

  int worldWidth;
  int worldHeight;

  BitmapFont font;
  Texture gearIcon;

  double fadeAmount = 1.0;

  SpriteBatch worldBatch;
  SpriteBatch screenBatch;

  PlayerCamera worldCamera;
  Camera2D screenCamera;

  bool ui = false;

  Texture black;
  
  Vector4 textColor = Vector4(254 / 255, 201 / 255, 237 / 255, 1.0);

  bool error = false;
  Vector4 errorColor = Vector4(135 / 255, 22 / 255, 70 / 255, 1.0);
  Vector4 tempError = Vector4.zero();

  Renderer(this.world, this.gl, this.screenWidth, this.screenHeight) {
    resize(screenWidth, screenHeight);
    worldBatch = new SpriteBatch.defaultShader(gl);
    screenBatch = new SpriteBatch.defaultShader(gl);
    font = assetManager.get("font");
    gearIcon = assetManager.get("atlas")["smallgear"];
    black = assetManager.get("atlas")["black"];
    Colors.rgbToHsl(errorColor, errorColor);
  }

  resize(num width, num height) {
    this.screenWidth = width;
    this.screenHeight = height;

    worldHeight = TILE_SIZE * TILE_HEIGHT;
    double aspectRatio = screenWidth / screenHeight;
    worldWidth = (worldHeight * aspectRatio).ceil();

    worldCamera = PlayerCamera(world, world.player, worldWidth, worldHeight);
    screenCamera = Camera2D.originBottomLeft(worldWidth, worldHeight);

    gl.setGLViewport(screenWidth, screenHeight);
  }

  render(World world, double delta) {
    worldCamera.update(delta);
    screenCamera.update();

    worldBatch.projection = worldCamera.combined;
    screenBatch.projection = screenCamera.combined;

    gl.clearScreen(Vector4(155 / 255 * (1 - fadeAmount), 160 / 255 * (1 - fadeAmount), 239 / 255 * (1 - fadeAmount), 1.0));

    worldBatch.color = Vector4(1 - fadeAmount, 1 - fadeAmount, 1 - fadeAmount, 1.0);
    worldBatch.begin();
    world.map.render(worldBatch, 0, 0, worldCamera.internal);
    for (var e in world.entities) {
      e.draw(worldBatch);
    }
    world.player.draw(worldBatch);
    worldBatch.end();

    if (ui) {
      screenBatch.color = Vector4(1 - fadeAmount, 1 - fadeAmount, 1 - fadeAmount, 1.0);
      screenBatch.begin();
      String display = "${world.player.pickups}/${world.numPickups}";
      int offset = font.measureWord(display);
      screenBatch.color = Vector4(screenBatch.color.r, screenBatch.color.b, screenBatch.color.g, 0.3);
      screenBatch.draw(black, 1, worldHeight - 13, width: offset + 12, height: 11);
      screenBatch.color = Vector4(screenBatch.color.r, screenBatch.color.b, screenBatch.color.g, 1.0);
      screenBatch.draw(gearIcon, 4 + offset, worldHeight - 11);
      if(error) {
        errorColor.z += delta;
        if(errorColor.z > 1) {
          errorColor.z = 0.3;
        }
        Colors.hslToRgb(errorColor, tempError);
        screenBatch.color.multiply(tempError);
        screenBatch.color = screenBatch.color;
      } else if(world.player.pickups == world.numPickups) {
        Colors.rgbToHsl(textColor, textColor);
        textColor.x += delta;
        if(textColor.x > 1) {
          textColor.x = 0;
        }
        Colors.hslToRgb(textColor, textColor);
        screenBatch.color.multiply(textColor);
        screenBatch.color = screenBatch.color;
      }
      font.drawWord(screenBatch, 3, worldHeight - 4, display);
      screenBatch.end();
    }
  }
}
