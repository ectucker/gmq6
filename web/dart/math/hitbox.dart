part of gmq;

class Hitbox {

  Aabb2 box;

  Hitbox(double x, double y, double width, double height) {
    box = Aabb2.minMax(Vector2(x, y), Vector2(x, y) + Vector2(width, height));
  }

  Vector2 get pos => box.min;
  set pos(Vector2 vec) => box.setCenterAndHalfExtents(vec + dimens / 2, dimens / 2);

  double get x => box.min.x;
  set x(double newX) => pos = Vector2(newX, y);
  double get y => box.min.y;
  set y(double newY) => pos = Vector2(x, newY);

  double get left => box.min.x;
  set left(double newX) => pos = Vector2(newX, y);
  double get bottom => box.min.y;
  set bottom(double newY) => pos = Vector2(x, newY);
  double get right => box.max.x;
  set right(double newX) => pos = Vector2(newX - width, y);
  double get top => box.max.y;
  set top(double newY) => pos = Vector2(x, newY - height);

  Vector2 get center => box.center;
  set center(Vector2 vec) => box.setCenterAndHalfExtents(vec, dimens / 2);
  double get centerX => center.x;
  set centerX(double newX) => center = Vector2(newX, centerY);
  double get centerY => center.y;
  set centerY(double newY) => center = Vector2(centerX, newY);

  int get tileX => (x / TILE_SIZE).floor();
  int get tileY => (y / TILE_SIZE).floor();

  int get tileLeft => (x / TILE_SIZE).floor();
  int get tileRight => ((x + width - 1) / TILE_SIZE).floor();
  int get tileBottom => (y / TILE_SIZE).floor();
  int get tileTop => ((y + height - 1) / TILE_SIZE).floor();

  Vector2 get dimens => Vector2(width, height);
  double get width => box.max.x - box.min.x;
  double get height => box.max.y - box.min.y;

  int get tileWidth => tileRight - tileLeft;
  int get tileHeight => tileTop - tileBottom;

  bool intersects(Hitbox other) {
    return other.box.intersectsWithAabb2(box);
  }

}