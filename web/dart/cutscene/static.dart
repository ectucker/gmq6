part of gmq;

class Static extends Entity {

  Texture texture;

  Static(World world, this.texture, int x, int y, int width, int height) : super(world, x, y, width, height);

  @override
  draw(SpriteBatch batch) {
    batch.draw(texture, box.x, box.y);
  }

}