part of gmq;

class Opening extends World {

  OpeningBot opening;
  Static shoe;

  bool started = false;
  bool transitioned = false;

  Opening(Game game, atlas, String mapName) : super(game, atlas, mapName) {
    player.enabled = false;
    opening = new OpeningBot(this, 88, 16, 8, 8);
    entities.add(opening);
    shoe = new Static(this, atlas['singleshoe'], 48, 16, 8, 8);
    entities.add(shoe);
  }

  @override
  update(double delta) {
    if(opening.stopTime > 1.0 && !started) {
      assetManager.get("powerup").playIfNot();
    }
    if (opening.stopTime > 1.2 && !started) {
      //player.box.left = opening.box.left;
      player.enabled = true;
      toRemove.add(opening);
      toRemove.add(shoe);
      Static tutorial = new Static(this, atlas['tutorial'], 56, 32, 48, 8);
      entities.add(tutorial);
      game.fadeIn();
      started = true;
    }

    if (player.box.right > 136) {
      game.fadeOut();
      player.frozen = true;
      new Tween()
        ..delay = 0.6
        ..callback = () {
          if (!transitioned) {
            game.world = new World(game, atlas, "blackbox.tmx");
            game.renderer =
                new Renderer(game.world, game.gl, game.width, game.height);
            game.renderer.ui = true;
            game.fadeIn();
            transitioned = true;
          }
        }
        ..start(game.tweenManager);
    }

    super.update(delta);
  }
}
