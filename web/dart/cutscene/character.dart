part of gmq;

class OpeningBot extends Entity {

  Texture texture;

  double stopTime = 0.0;
  bool stopped = false;

  double frameTime = 0.0;
  int frame = 1;

  OpeningBot(World world, int x, int y, int width, int height) : super(world, x, y, width, height) {
    texture = assetManager.get("atlas")["solobot1"];
  }

  @override
  update(double delta) {
    super.update(delta);
    velocity.x = -8;
    if(box.x < 6 * TILE_SIZE + 4) {
      velocity.x = 0;
      stopTime += delta;
    }
    if(stopTime > 0.2 && !stopped) {
      world.game.fadeOut();
      stopped = true;
    }

    frameTime += delta;
    if(frameTime > 0.2) {
      frame++;
      frameTime = 0;
    }
    if(frame > 4) {
      frame = 1;
    }
    texture = world.atlas['solobot$frame'];
  }

  @override
  draw(SpriteBatch batch) {
    batch.draw(texture, box.x, box.y);
  }

}