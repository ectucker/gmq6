library gmq;

import 'package:cobblestone/cobblestone.dart';
import 'dart:html';

part 'game.dart';
part 'graphics/renderer.dart';
part 'world/world.dart';
part 'world/entity.dart';
part 'math/hitbox.dart';
part 'graphics/player_camera.dart';
part 'world/player.dart';
part 'world/pickup.dart';
part 'world/endcheck.dart';
part 'cutscene/cutscene.dart';
part 'cutscene/character.dart';
part 'cutscene/static.dart';

void main() {
  var game = new Game();
}
