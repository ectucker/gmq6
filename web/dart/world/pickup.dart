part of gmq;

class Item extends Entity {

  var atlas;

  Texture texture;

  Item(World world, this.atlas, double x, double y, int width, int height) : super(world, x, y, width, height) {
    int type = rand.nextInt(3);
    switch(type) {
      case 0:
        texture = atlas["smallgear"];
        break;
      case 1:
        texture = atlas["ic"];
        break;
      case 2:
        texture = atlas["bolt1"];
        break;
    }
    box.y += 1;
    tweenUp();
  }

  tweenUp() {
    Tween()
      ..get = [() => box.y]
      ..set = [(val) => box.y = val]
      ..target = [box.y + 1]
      ..duration = 0.3
      ..ease = Ease.cubicIn
      ..callback = tweenDown
      ..start(world.game.tweenManager);
  }

  tweenDown() {
    Tween()
      ..get = [() => box.y]
      ..set = [(val) => box.y = val]
      ..target = [box.y - 1]
      ..duration = 0.3
      ..ease = Ease.cubicIn
      ..callback = tweenUp
      ..start(world.game.tweenManager);
  }

  @override
  draw(SpriteBatch batch) {
    batch.draw(texture, box.x, box.y);
  }

  @override
  update(double delta) {
    if(box.intersects(world.player.box)) {
      world.player.pickups++;
      assetManager.get("pickup").play();
      world.toRemove.add(this);
    }
  }

}