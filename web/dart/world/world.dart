part of gmq;

const double X__FORWARD_ACCELERATION = 0.08;
const double X_SLOW_ACCELERATION = 0.02;
const double X__AIR_SLOW_ACCELERATION = 0.01;
const double G_ACCELERATION = 0.05;

const double PLANCK_VELOCITY = 0.001;

class World {
  Game game;

  var atlas;

  Tilemap map;

  List<Entity> entities = [];
  List<Entity> toRemove = [];

  Player player;

  int numPickups = 0;

  int endPhase = 0;
  double endTime = 0.0;

  Sound doubleBeep;
  double beepTime = 0.3;

  World(this.game, this.atlas, String mapName) {
    map = game.assetManager.get(mapName);

    ObjectGroup stuff = map.objectGroups.first;
    for(MapObject thing in stuff.objects) {
      if(thing.properties.hasProp("end") && thing.properties["end"] && thing is MapRect) {
        entities.add(new Endcheck(this, thing.pos.x, thing.pos.y, thing.width, thing.height));
      } else if(thing.properties.hasProp("playerspawn") && thing.properties["playerspawn"]) {
        player = new Player(this, atlas, thing.pos.x, thing.pos.y);
      } else {
        entities.add(new Item(this, atlas, thing.pos.x, thing.pos.y, 8, 8));
        numPickups++;
      }
    }

    doubleBeep = assetManager.get("doublebeep");
  }

  update(double delta) {
    player.update(delta);
    for(var e in entities) {
      e.update(delta);
    }
    for(var e in toRemove) {
      entities.remove(e);
    }
    toRemove.clear();

    if(endPhase > 0) {
      endTime += delta;
      if(endTime > 0.6 && endPhase == 1) {
        Static newPlayer = new Static(this, atlas['skatebot1'], 946 + 200, 88, 8, 8);
        entities.add(newPlayer);
        for(int i = 0; i < 50; i++) {
          Static bot = new Static(this, atlas['solobot1'], 981 + 200 + i * 8, 88, 8, 8);
          entities.add(bot);
        }

        player.enabled = false;
        game.renderer.ui = false;

        endPhase++;
      }
      if(endTime > 7 && endPhase == 2) {
        game.fadeIn();
        endPhase++;
      }
      if(endTime > 7.8 && endPhase == 3) {
        doubleBeep.play();
        endPhase++;
      }
      if(endTime > 8.2 && endPhase >= 4) {
        player.box.x += delta * 10;
        beepTime -= delta;
        if(beepTime < 0) {
          doubleBeep.play();
          beepTime = 1 / (0.2 * (endTime - 5));
        }
      }
      if(endTime > 28.2 && endPhase == 4) {
        game.fadeOut();
        endPhase++;
      }
      if(endTime > 28.7) {
        endPhase = 0;
      }
    }
  }

  startEndScene() {
    endPhase = 1;

    game.fadeOut();
    game.audio.stopAll();

    player.frozen = true;
    player.burnerOn = false;
    player.gliding = false;
    player.jumpTime = 0.2;
    player.boostTime = 0.0;

    assetManager.get("factory").play();
  }
}
