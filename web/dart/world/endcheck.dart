part of gmq;

class Endcheck extends Entity {

  var atlas;

  Texture texture;

  Endcheck(World world, double x, double y, double width, double height) : super(world, x, y, width, height);

  @override
  draw(SpriteBatch batch) {}

  @override
  update(double delta) {
    if(box.intersects(world.player.box)) {
      if(world.player.pickups == world.numPickups) {
        world.startEndScene();
        world.toRemove.add(this);
      }
      world.game.renderer.error = true;
    } else {
      world.game.renderer.error = false;
    }
  }

}