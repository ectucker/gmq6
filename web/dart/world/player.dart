part of gmq;

class Player extends Entity {

  var atlas;
  
  Keyboard keyboard;

  Texture mainTexture;
  int mainFrame = 1;
  double mainTime = 0;

  Texture burnerTexture;
  bool burnerOn = false;
  int burnerFrame = 1;
  double burnerTime = 0;

  Texture boostTexture;

  Texture jumpTexture;
  double jumpTime = 5.0;

  Texture glideTexture;

  Vector2 targetVelocity = Vector2.zero();

  double boostTime = 0;
  bool gliding = false;

  int pickups = 0;

  double lastOnGround = 0.0;

  bool enabled = true;
  bool frozen = false;

  Player(World world, this.atlas, double x, double y): super(world, x, y, 10, 12) {
    keyboard = world.game.keyboard;
  }
  
  @override
  update(double delta) {
    if(enabled && !frozen) {
      if (onGround()) {
        lastOnGround = 0.0;
      } else {
        lastOnGround += delta;
      }

      control(delta);

      if (targetVelocity.x != 0) {
        velocity.x = X__FORWARD_ACCELERATION * targetVelocity.x +
            (1 - X__FORWARD_ACCELERATION) * velocity.x;
      } else {
        if (onGround()) {
          velocity.x = X_SLOW_ACCELERATION * targetVelocity.x +
              (1 - X_SLOW_ACCELERATION) * velocity.x;
        } else {
          velocity.x = X__AIR_SLOW_ACCELERATION * targetVelocity.x +
              (1 - X__AIR_SLOW_ACCELERATION) * velocity.x;
        }
      }
      if (boostTime > 0 && velocity.y <= 0) {
        velocity.y = 0;
      } else {
        velocity.y =
            G_ACCELERATION * targetVelocity.y +
                (1 - G_ACCELERATION) * velocity.y;
      }

      super.update(delta);

      boostTime -= delta;

      mainTime += delta;
      if (mainTime > 1 / (velocity.x.abs() / 5) && onGround()) {
        mainFrame++;
        mainTime = 0;
      }
      if (mainFrame > 4) {
        mainFrame = 1;
      }

      burnerTime += delta;
      if (burnerTime > 0.2) {
        burnerFrame++;
        burnerTime = 0;
      }
      if (burnerFrame > 2) {
        burnerFrame = 1;
      }

      jumpTime += delta;
    }

      mainTexture = atlas["skatebot$mainFrame"];
      burnerTexture = atlas["backflame$burnerFrame"];
      boostTexture = atlas["boost"];
      jumpTexture = atlas["jump"];
      glideTexture = atlas["glide"];
  }
  
  control(double delta) {
    if (keyboard.keyPressed(KeyCode.RIGHT) ==
        keyboard.keyPressed(KeyCode.LEFT)) {
      assetManager.get("move").stop();
      targetVelocity.x = 0;
      burnerOn = false;
    }
    if (keyboard.keyPressed(KeyCode.RIGHT)) {
      targetVelocity.x = 200;
      flip = false;
      assetManager.get("move").playIfNot(loop: true);
      burnerOn = true;
    } else if (keyboard.keyPressed(KeyCode.LEFT)) {
      targetVelocity.x = -200;
      flip = true;
      assetManager.get("move").playIfNot(loop: true);
      burnerOn = true;
    }

    if (keyboard.keyJustPressed(KeyCode.SPACE) && boostTime <= 0) {
      boostTime = 0.2;
      assetManager.get("boost").play();
      if (flip) {
        velocity.x = -400;
      } else {
        velocity.x = 400;
      }
    }

    gliding = keyboard.keyPressed(KeyCode.SHIFT);

    if (keyboard.keyJustPressed(KeyCode.UP) && lastOnGround < 0.01 && jumpTime > 0.2) {
      assetManager.get("boost").play();
      velocity.y = 280;
      jumpTime = 0.0;
    }

    if (velocity.y > -200) {
      if (gliding) {
        targetVelocity.y = -30;
      } else {
        targetVelocity.y = -100;
      }
    } else {
      targetVelocity.y = 0;
    }
  }

  @override
  draw(SpriteBatch batch) {
    if(enabled) {
      batch.draw(mainTexture, box.x, box.y, flipX: flip);
      if (burnerOn) {
        batch.draw(burnerTexture, box.x + (flip ? 9 : -4), box.y, flipX: flip);
      }
      if (boostTime > 0.05) {
        batch.draw(boostTexture, box.x + (flip ? 9 : -11), box.y, flipX: flip);
      }
      if (jumpTime < 0.15) {
        batch.draw(jumpTexture, box.x, box.y - 5, flipX: flip);
      }
      if (gliding) {
        batch.draw(glideTexture, box.x, box.y + 7, flipX: flip);
      }
    }
  }

}