part of gmq;

class Entity {
  World world;
  Hitbox box;

  Vector2 velocity = Vector2.zero();
  bool flip = false;

  Entity(this.world, num x, num y, num width, num height) {
    box = new Hitbox(
        x.toDouble(), y.toDouble(), width.toDouble(), height.toDouble());
  }

  bool isSolid(int tileX, int tileY) {
    for (TileLayer layer in world.map.layers) {
      if (tileX >= 0 &&
          tileX < layer.width &&
          tileY >= 0 &&
          tileY < layer.height) {
        var tile = layer.getTile(tileX, tileY);
        if (tile != null) {
          if (tile.properties.hasProp("solid") && tile.properties["solid"]) {
            return true;
          }
          if (tile.properties.hasProp("oneway") &&
              tile.properties["oneway"] &&
              box.bottom >= tileY * TILE_SIZE + TILE_SIZE) {
            return true;
          }
        }
      } else {
        return true;
      }
    }
    return false;
  }

  double calcMaxX() {
    for (int i = box.tileRight; i <= box.tileRight + 4; i++) {
      for (int j = box.tileBottom; j <= box.tileTop; j++) {
        if (isSolid(i, j)) {
          return (i * TILE_SIZE).toDouble();
        }
      }
    }
    return ((box.tileRight + 4) * TILE_SIZE).toDouble();
  }

  double calcMinX() {
    for (int i = box.tileLeft; i >= box.tileLeft - 4; i--) {
      for (int j = box.tileBottom; j <= box.tileTop; j++) {
        if (isSolid(i, j)) {
          return (i * TILE_SIZE + TILE_SIZE).toDouble();
        }
      }
    }
    return ((box.tileLeft - 4) * TILE_SIZE + TILE_SIZE).toDouble();
  }

  double calcMaxY() {
    for (int i = box.tileTop; i <= box.tileTop + 4; i++) {
      for (int j = box.tileLeft; j <= box.tileRight; j++) {
        if (isSolid(j, i)) {
          return (i * TILE_SIZE).toDouble();
        }
      }
    }
    return ((box.tileTop + 4) * TILE_SIZE).toDouble();
  }

  double calcMinY() {
    for (int i = box.tileBottom; i >= box.tileBottom - 2; i--) {
      for (int j = box.tileLeft; j <= box.tileRight; j++) {
        if (isSolid(j, i)) {
          return (i * TILE_SIZE + TILE_SIZE).toDouble();
        }
      }
    }
    return ((box.tileBottom - 2) * TILE_SIZE + TILE_SIZE).toDouble();
  }

  bool onGround() {
    bool grounded = false;
    for (int i = box.tileLeft; i <= box.tileRight; i++) {
      if (box.y - box.tileBottom * TILE_SIZE < 1 &&
          isSolid(i, box.tileBottom - 1)) {
        grounded = true;
      }
    }
    return grounded;
  }

  update(double delta) {
    double minX = calcMinX();
    double maxX = calcMaxX();

    if (box.right + velocity.x * delta > maxX) {
      box.right = maxX;
      velocity.x = 0;
    } else if (box.left + velocity.x * delta < minX) {
      box.left = minX;
      velocity.x = 0;
    } else {
      box.x += velocity.x * delta;
    }

    if (velocity.x.abs() < PLANCK_VELOCITY) {
      velocity.x = 0;
    }
    if (velocity.y.abs() < PLANCK_VELOCITY) {
      velocity.y = 0;
    }

    double minY = calcMinY();
    double maxY = calcMaxY();

    if (box.top + velocity.y * delta > maxY) {
      box.top = maxY;
      velocity.y = 0;
    } else if (box.bottom + velocity.y * delta < minY) {
      box.bottom = minY;
      velocity.y = 0;
    } else {
      box.y += velocity.y * delta;
    }
  }

  draw(SpriteBatch batch) {}
}
